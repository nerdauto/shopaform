<?php namespace Nerd\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteNerdShopaholicProduct extends Migration
{
    public function up()
    {
        Schema::dropIfExists('nerd_shopaholic_product');
    }
    
    public function down()
    {
        Schema::create('nerd_shopaholic_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('year')->nullable()->unsigned();
            $table->string('model_product', 255)->nullable();
            $table->integer('product_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('related_product', 255)->nullable();
            $table->integer('popularity')->nullable()->default(0);
        });
    }
}
