<?php namespace Nerd\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNerdShopaholicType extends Migration
{
    public function up()
    {
        Schema::create('nerd_shopaholic_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titre', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nerd_shopaholic_type');
    }
}
