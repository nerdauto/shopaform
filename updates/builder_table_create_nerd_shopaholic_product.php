<?php namespace Nerd\Shopaholic\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNerdShopaholicProduct extends Migration
{
    public function up()
    {
        Schema::create('nerd_shopaholic_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('year')->nullable()->unsigned();
            $table->string('model_product', 255)->nullable();
            $table->integer('product_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('related_product', 255)->nullable();
            $table->integer('popularity')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nerd_shopaholic_product');
    }
}
