import CartLinkHeader from '/plugins/nerd/shopaform/assets/js/cart-link-header.js';
import CartPositionList from "/plugins/nerd/shopaform/assets/js/cart-position-list.js";
export default new class ButtonAddToCart {
  constructor() {
    //Init selectors
    this.btnAddToCart = 'btn-add-to-cart';
    //Add events
    this.eventHandlers();
  }

  eventHandlers() {
  
  }
  

  addOfferToCart(form, button) {
    console.log("TEST");
    CartPositionList.sendRequestUpdateItem($btn);

    const cartData = [
      {
        'offer_id': form.find('input[name="offer_id"]').val(),
        'quantity': form.find('select[name="quantity"]').val(),
      }
    ];

    $.request('Cart::onAdd', {
      data: {'cart': cartData},
      success: function (response) {
        if (!!response && response.status) {
          button.attr('data-content', 'Item added to cart');
          CartLinkHeader.updateBlock();
        } else {
          button.attr('data-content', response.message);
        }

        button.popover('show');
        setTimeout(() => {
          button.popover('hide');
        }, 1500);
      }
    });
  }
}();
