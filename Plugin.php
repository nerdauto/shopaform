<?php namespace Nerd\Shopaform;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Nerd\Shopaform\Components\pieceform'        => 'pieceform',
            'Nerd\Shopaform\Components\selecttype'        => 'selecttype',
            'Nerd\Shopaform\Components\selectpiece'     => 'selectpiece'
        ];
    }

    public function registerSettings()
    {
    }
}
