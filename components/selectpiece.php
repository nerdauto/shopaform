<?php

namespace nerd\Shopaform\Components;

use Cms\Classes\ComponentBase;
use Db;
use System\Models\File;
use GuzzleHttp\Psr7\Request;
use Input;
use Redirect;
use Session;

class selectpiece extends ComponentBase
{

    
    public function componentDetails()
    {
        return [
            'name' => 'selectpiece',
            'description' => 'Inventaire du résultat de recherche'
        ];
    }


    public function defineProperties()
    {
        // return [
        //     "popularityFilter" => [
        //         "title" => "Add filter by populairity",
        //         "type" => "checkbox",
        //         'default' => 'false',
        //     ],
        // ];
    }

    public function Displaysearch()
    {

        $category = strtolower($_GET['category']);
        $year = strtolower($_GET['year']);
        $model = strtolower($_GET['model']);
        $type_id = strtolower($_GET['type']);
        

        //Get all products from search results
        $obProduct = Db::select("SELECT DISTINCT lovata_shopaholic_products.id FROM  lovata_shopaholic_products  INNER JOIN nerd_shopaholic_type on lovata_shopaholic_products.type_id = nerd_shopaholic_type.id where category_id = $category AND year = $year AND model_product = '$model' AND type_id = $type_id");
    
       $ProductArray =array();
        $result = array();
        $MesProduits = array();
        $test = 0;
        foreach ( $obProduct as $value) {
          //  if($test = $value) continue;
            $test = $value;
           $ProductArray  = get_object_vars($value);
                array_push($MesProduits, $ProductArray['id']);
        }

        //Get the selected type
        $obType = Db::select("SELECT nerd_shopaholic_type.id FROM nerd_shopaholic_type WHERE id = $type_id");

        $TypeArray =array();
        $MonType = array();
        foreach ( $obType as $value) {
           $TypeArray  = get_object_vars($value);
                array_push($MesProduits, $TypeArray['id']);
        }

        $result = ['Product' => $MesProduits , 'Type' => $TypeArray] ;
       // $sValue = $obSetting['id'];
        return $result;
    }

    public function makeItem($id , $source = 'lovata_shopaholic_products')
    {


        

        $obSetting = Db::select("SELECT * FROM $source WHERE id = $id");
        
        if ($source != "lovata_shopaholic_products")
        {
            $obImage = File::where('attachment_type', 'Nerd\Shopaholic\Models\Type')->where('field','image')->where('attachment_id' , $id)->first();
            $obSetting += ['myImage' => $obImage];
        }
        else
        {
            $obOffer = Db::select("SELECT lovata_shopaholic_offers.id  , old_price , price FROM lovata_shopaholic_offers INNER JOIN lovata_shopaholic_prices on lovata_shopaholic_offers.id = lovata_shopaholic_prices.item_id   WHERE product_id = $id");
            $obSetting += ['offer' => $obOffer];
        }
     
        return $obSetting;
       
    }

    public function onRun()
    {

        $this->page['year'] = strtolower($_GET['year']);
          $this->page['modele'] = strtolower($_GET['model']);
          $this->page['category'] = strtolower($_GET['category']);
        $this->addCss('/plugins/nerd/shopaform/assets/css/step2.css' , 'core');
        $this->addJs('/plugins/nerd/shopaform/assets/js/step2.js' , 'core');
         }


    public function getCategory($id )
    {


        

        $obSetting = Db::select("SELECT * FROM lovata_shopaholic_categories WHERE id = $id");
        
        
     
        return $obSetting;
       
    }

    public function getProperty($propertyName)
    {
        return $this->property($propertyName);
    }

    public function get($sCode, $nIdCategory)
    {
        $type = Input::get('year') ?? '';
        switch ($sCode) {
            case "year":
                $obSetting = Db::select("select DISTINCT year from lovata_shopaholic_products  where category_id ='$nIdCategory' and type_Vehicule='$type'");
                break;
            case "type_Vehicule":
                $obSetting = Db::select("select DISTINCT type_Vehicule from lovata_shopaholic_products  where category_id ='$nIdCategory'");
                break;
        }

        $MesProduits = array();
        foreach ($obSetting as $value) {
            $objectToArray  = get_object_vars($value);
            array_push($MesProduits, $objectToArray[$sCode]);
        }
        return $MesProduits;
    }

    public function getProductListFiltered($nIdCategory, $filter, $IdList)
    {
        switch ($filter) {
            case 0:
                $sCondition = "ORDER BY popularity DESC";
                break;
            case 1:
                $sCondition = "ORDER BY popularity ASC";
                break;
            case 2:
                $sCondition = "ORDER BY year DESC";
                break;
            case 3:
                $sCondition = "ORDER BY year ASC";
                break;
            case 4:
                $sCondition = "ORDER BY lovata_shopaholic_prices.price DESC";
                break;
            case 5:
                $sCondition = "ORDER BY lovata_shopaholic_prices.price ASC";
                break;
            default:
                break;
        }
        $obSetting = Db::select("SELECT * from lovata_shopaholic_products  where category_id ='$nIdCategory'  AND id IN $IdList " . $sCondition );
        return $obSetting;
    }

    public function onFilterList()
    {
        $filter = Request('filter');
        $maListe = Session::get('IdList');
        
        $typeUpdate = 'filter';
        return [
            '#TEST' => $this->renderPartial('@partials/productsList.htm', ['filter' => $filter, 'typeUpdate' => $typeUpdate , 'listId' => $maListe])
        ];
    }
    
    function has_dupes($array) {
    $dupe_array = array();
    foreach ($array as $val) {
        if (++$dupe_array[$val] > 1) {
            return true;
        }
    }
    return false;
}
}
