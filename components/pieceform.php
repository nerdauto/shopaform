<?php

namespace Nerd\Shopaform\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use Cms\Models\ThemeData;
use Db;
use ValidationException;
use Validator;
use Redirect;
use Session;
use Input;
use Sabberworm\CSS\Property\Selector;

class pieceform extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'pieceform',
            'description' => 'Formulaire de pièce',
        ];
    }

    public function defineProperties()
    {
    }


    public function onRun()
    {
        $this->addCss('/plugins/nerd/shopaform/assets/css/step1.css','core');
        $this->addJs('/plugins/nerd/shopaform/assets/js/step1.js','core');
        
    }


    public function getProperty($propertyName)
    {
        return $this->property($propertyName);
    }

    public  function onSearch()
    {

        $year = Request('year');
        $model = request('model');
        $category = request('category');     
        $MyLink = "/type-inventaire/?category=$category&year=$year&model=$model";

            return Redirect::to($MyLink);
        
    }




    public function get($sCode, $nIdCategory = 1)
    {
        $year = Input::get('year') ?? '';
        $model = Input::get('model') ?? '';

        switch ($sCode) {
            case "year":
                $obSetting = Db::select("SELECT DISTINCT year from lovata_shopaholic_products  where category_id ='$nIdCategory' ORDER BY year DESC");
                break;
            case "model_product":
                $obSetting = Db::select("SELECT DISTINCT model_product from lovata_shopaholic_products  where category_id ='$nIdCategory'");
                break;
            case "type_Vehicule":
                $obSetting = Db::select("select DISTINCT type_Vehicule from lovata_shopaholic_products  where category_id ='$nIdCategory'");
                break;
        }

        $MesProduits = array();
        foreach ($obSetting as $value) {
            $objectToArray  = get_object_vars($value);
            array_push($MesProduits, $objectToArray[$sCode]);
        }
        return $MesProduits;
    }
    
    
    
     public function onTest()
    {
      $test =5;
        $categorie = Request('recipient');
      return [
            '#myForm' =>
            $this->renderPartial('@partials/form.htm', [ 'categorie' =>$categorie
             
            ])
        ];
    }

}
